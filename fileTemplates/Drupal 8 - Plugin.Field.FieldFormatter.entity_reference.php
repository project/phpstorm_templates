#parse("Drupal 8 - Plugin.Field.FieldFormatter.php")
<?php

namespace ${NAMESPACE};

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Url;

/**
 * @\Drupal\Core\Field\Annotation\FieldFormatter(
 *   id = "${id}",
 *   label = @\Drupal\Core\Annotation\Translation("${NAME}"),
 *   description = @\Drupal\Core\Annotation\Translation("Useless description"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class ${NAME} extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface ${DS}items, ${DS}langcode) {
    ${DS}elements = [];

    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface ${DS}items */
    foreach (${DS}this->getEntitiesToView(${DS}items, ${DS}langcode) as ${DS}delta => ${DS}entity) {
      ${DS}elements[${DS}delta] = [
        '#type' => 'markup',
        '#markup' => ${DS}entity->label(),
      ];
    }

    return ${DS}elements;
  }

}
