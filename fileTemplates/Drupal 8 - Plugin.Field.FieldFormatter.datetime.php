#parse("Drupal 8 - Plugin.Field.FieldFormatter.php")
<?php

namespace ${NAMESPACE};

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\datetime\Plugin\Field\FieldFormatter\DateTimeFormatterBase;

/**
 * @Drupal\Core\Field\Annotation\FieldFormatter(
 *   id = "${id}",
 *   label = @Drupal\Core\Annotation\Translation("${NAME}"),
 *   field_types = {
 *     "datetime"
 *   }
 * )
 */
class ${NAME} extends DateTimeFormatterBase {
  
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface ${DS}items, ${DS}langcode) {
    ${DS}elements = [];

    foreach (${DS}items as ${DS}delta => ${DS}item) {
      if (${DS}item->date) {
        /** @var \Drupal\Core\Datetime\DrupalDateTime ${DS}date */
        ${DS}date = ${DS}item->date;
        ${DS}elements[${DS}delta] = [
          '#markup' => ${DS}this->formatDate(${DS}date),
        ];
      }
    }

    return ${DS}elements;
  }
  
    /**
   * {@inheritdoc}
   */
  protected function formatDate(${DS}date) {
    return ${DS}date->format('%y');
  }

}
