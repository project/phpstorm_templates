<?php

namespace ${NAMESPACE};

use \Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface ${NAME}.
 */
interface ${NAME} extends ConfigEntityInterface {}
